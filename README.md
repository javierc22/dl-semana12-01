# Semana 12 - Ejemplo 1

### Asset path

1. Introducción
2. Introducción a Helpers
3. Utilizando el helper 'image_tag'
4. Helper 'asset_path'

### Sprockets

1. Sprockets
2. Organizando nuestros assets
3. Agregando una carpeta al asset path
4. Manifiesto CSS

### Integración de una plantilla

1. Integrando una plantilla Parte 1
2. Integrando una plantilla Parte 2
3. Integrando una plantilla Parte 3
4. Integrando una plantilla Parte 4
5. Integrando una plantilla Parte 5

### Pre-Procesadores

1. Introducción a SASS
2. Nesting y Namespaces
3. Parciales e Import
4. Mixins
5. Bootstrap SASS
6. Bootstrap variables
7. Slim
8. Jquery en Rails 5.1
